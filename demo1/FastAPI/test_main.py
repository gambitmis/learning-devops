from fastapi.testclient import TestClient
from fastapi import status
from main import app

client=TestClient(app)

def test_index_returns_correct():
    response = client.get('/')
    
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == {"message":"Hello"}
    
def test_create_transaction():
    transaction_data = {
        "amount": 999,
        "category": "pytest",
        "description": "pytest",
        "is_income": False,
        "date": "2023-09-22"
    }
    response = client.post('/transactions', json=transaction_data)
    assert response.status_code == 200
    assert "id" in response.json()
    assert response.json()["amount"] == 999
    
def test_list_transaction():
    response = client.get('/transactions')
    assert response.status_code == 200
    
    